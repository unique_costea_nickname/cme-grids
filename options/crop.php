<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
* Renders Crop Templates List
*/
class CME_Default_Crop {
  private $icon_path;

  /**
   * __construct
   * Class Constructor
   */
  function __construct() {
    $this->icon_path = plugins_url( '../dist/admin/img/crop_options/', __FILE__);
  }


  /**
   * show_crop_options return html
   */
  public function show_crop_options() {
    ob_start();
    $this->build_html();
    $options = ob_get_clean();
    echo $options;
  }


  /**
   * get_crop_options contains all options we have currently
   * @return Array Containing all existing options
   */
  private function get_crop_options() {
    return array(
      array(
        'icon' => '32.png',
        'name' => '3:2',
        'height' => '66',
      ),
      array(
        'icon' => '43.png',
        'name' => '4:3',
        'height' => '75',
      ),
      array(
        'icon' => '54.png',
        'name' => '5:4',
        'height' => '80',
      ),
      array(
        'icon' => '11.png',
        'name' => '1:1',
        'height' => '100',
      ),
      array(
        'icon' => '45.png',
        'name' => '4:5',
        'height' => '125',
      ),
      array(
        'icon' => '34.png',
        'name' => '3:4',
        'height' => '133',
      ),
      array(
        'icon' => '23.png',
        'name' => '2:3',
        'height' => '150',
      )
    );
  }


  /**
   * build_html builds html that will be rendered in our custom metabox
   */
  private function build_html() {
    $options = $this->get_crop_options();

    echo '<div class="cme-grid__wrp-box">';
    echo '<h3>' . _('Choose one option for your layout!') . '</h3>';
    echo '<ul data-default-crop-options class="cme-grid__templates-list">';
    foreach ($options as $key => $option) {
      echo '<li class="cme-grid__templates-item">';
        echo '<input data-crop-option id="cme-grid-crop-' . $key . '" type="radio" name="cme-grid-crop-option" value=' . $option['height'] . '>';
        echo '<label class="cme-grid__label" for="cme-grid-crop-' . $key . '"> <img class="cme-grid__label-img" src="' . $this->icon_path . $option['icon'] . '" alt="' . _($option['name']) . '" />' . _($option['name']) . '</label>';
      echo '</li>';
    }
    echo '</ul>';
    echo '</div>';
  }
}
