<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class for Custom Widget Metabox inside Admin Panel
 */
class CME_MetaBoxes {
  private $meta_id;
  private $meta_title;
  private $context;
  private $priority;
  private $page;
  public $meta_keys;


  /**
   * __construct Class constructor
   */
  public function __construct() {
    $this->init_vars();

    $this->hook_metaboxes();
  }


  /**
   * init_vars Initialization of local variables
   */
  private function init_vars() {
    $this->meta_id = 'cme_grid_options';
    $this->meta_title = 'Grid Options';
    $this->context = 'side';
    $this->priority = 'default';
    $this->page = 'cme_grids';

    $this->meta_keys = new stdClass();
    $this->meta_keys->lightbox = 'cme_grid_lightbox';
    $this->meta_keys->lazyload = 'cme_grid_lazyload';
  }


  /**
   * hook_metaboxes Metabox hooks to be rendered into Admin Panel
   */
  private function hook_metaboxes() {
    add_action( 'load-post.php', array( $this, 'metaboxes_setup' ));
    add_action( 'load-post-new.php', array( $this, 'metaboxes_setup' ));
  }


  /**
   * metaboxes_setup Hooks when the metabox values should be save into DB
   */
  public function metaboxes_setup() {
    add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
    add_action( 'save_post', array( $this, 'save_metabox'));
  }


  /**
   * add_meta_boxes Hook to add rendered metabox
   */
  public function add_meta_boxes () {
    add_meta_box(
      $this->meta_id,
      $this->meta_title,
      array( $this, 'render_custom_metabox'),
      $this->page,
      $this->context,
      $this->priority
    );
  }


  /**
   * render_custom_metabox Metabox HTML
   * @param  Post $object [description]
   */
  public function render_custom_metabox($object) {
    wp_nonce_field( basename( __FILE__ ), 'cme_grid_nonce' );
    $lightbox = get_post_meta($object->ID, $this->meta_keys->lightbox, true );
    $lazyload = get_post_meta($object->ID, $this->meta_keys->lazyload, true );

    ?>
      <p>
        <input
            class="widefat"
            type="checkbox"
            name="<?php echo $this->meta_keys->lightbox ?>"
            id="<?php echo $this->meta_keys->lightbox ?>"
            value="enable"
            <?php
              if ($lightbox == 'enable') {
                echo "checked=\"checked\"";
              }
            ?>
          />
        <label for="<?php echo $this->meta_keys->lightbox ?>"><?php _e( "Enable lightbox for this grid", 'cme_grids' ); ?></label>
      </p>

      <p>
        <input
            class="widefat"
            type="checkbox"
            name="<?php echo $this->meta_keys->lazyload ?>"
            id="<?php echo $this->meta_keys->lazyload ?>"
            value="enable"
            <?php
              if ($lazyload == 'enable') {
                echo "checked=\"checked\"";
              }
            ?>
          />
        <label for="<?php echo $this->meta_keys->lazyload ?>"><?php _e( "Enable lazyloading for images in this grid", 'cme_grids' ); ?></label>
      </p>
    <?php
  }


  /**
   * save_metabox update meta values in DB
   * @param  int $post_id post_id of for post to be updated
   */
  public function save_metabox($post_id) {

    /* Verify the nonce before proceeding. */
    if ( !isset( $_POST['cme_grid_nonce'] ) || !wp_verify_nonce( $_POST['cme_grid_nonce'], basename( __FILE__ ) ) )
      return $post_id;

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
      return;

    if ( !current_user_can( 'edit_posts' ) )
      return;

    $new_lightbox = false;
    $new_lazyload = false;

    if (isset( $_POST[$this->meta_keys->lightbox]) ) {
      $new_lightbox = $_POST[$this->meta_keys->lightbox];
    }

    if (isset( $_POST[$this->meta_keys->lazyload]) ) {
      $new_lazyload = $_POST[$this->meta_keys->lazyload];
    }

    $lightbox = get_post_meta( $post_id, $this->meta_keys->lightbox, true );
    $lazyload = get_post_meta( $post_id, $this->meta_keys->lazyload, true );


    if ($new_lightbox) {
        if ($lightbox != $new_lightbox) {
          add_post_meta( $post_id, $this->meta_keys->lightbox, $new_lightbox );
        }
    } else {
        delete_post_meta( $post_id, $this->meta_keys->lightbox );
    }

    if ($new_lazyload) {
      if ($new_lazyload !== $lazyload) {
        add_post_meta( $post_id, $this->meta_keys->lazyload, $new_lazyload );
      }
    } else {
        delete_post_meta( $post_id, $this->meta_keys->lazyload);
    }
  }
};
