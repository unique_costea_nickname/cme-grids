<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Class responsible for redndering and showing premade templates
 */
class CME_Default_Grids {
  private $icon_path;


  /**
   * __construct class construtor where all local variables are initiated
   */
  function __construct() {
    $this->icon_path = plugins_url( '../dist/admin/img/grids/', __FILE__);
  }


  /**
   * show_templates return html
   */
  public function show_templates() {
    ob_start();
    $this->build_html();
    $templates = ob_get_clean();
    echo $templates;
  }


  /**
   * get_templates_list contains all options we have currently
   * @return Array Containing all existing options
   */
  private function get_templates_list() {
    return array(
      array(
        'icon' => 'split_x.png',
        'name' => 'Split Horizontal',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"","height":"50%","left":"","top":"0px"},{"width":"","height":"50%","left":"","top":"50%"}]}',
      ),
      array(
        'icon' => 'split_y.png',
        'name' => 'Split Vertical',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"50%","height":"","left":"0px","top":""},{"width":"50%","height":"","left":"50%","top":""}]}',
      ),
      array(
        'icon' => 'triple_left.png',
        'name' => 'Three boxes left',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"50%","height":"","left":"0px","top":"","childs":[{"width":"","height":"50%","left":"","top":"0px"},{"width":"","height":"50%","left":"","top":"50%"}]},{"width":"50%","height":"","left":"50%","top":""}]}'
      ),
      array(
        'icon' => 'triple_right.png',
        'name' => 'Three boxes right',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"50%","height":"","left":"0px","top":""},{"width":"50%","height":"","left":"50%","top":"","childs":[{"width":"","height":"50%","left":"","top":"0px"},{"width":"","height":"50%","left":"","top":"50%"}]}]}'
      ),
      array(
        'icon' => 'triple_bottom.png',
        'name' => 'Three boxes bottom',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"","height":"50%","left":"","top":"0px"},{"width":"","height":"50%","left":"","top":"50%","childs":[{"width":"50%","height":"","left":"0px","top":""},{"width":"50%","height":"","left":"50%","top":""}]}]}'
      ),
      array(
        'icon' => 'triple_top.png',
        'name' => 'Three boxes top',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"","height":"50%","left":"","top":"0px","childs":[{"width":"50%","height":"","left":"0px","top":""},{"width":"50%","height":"","left":"50%","top":""}]},{"width":"","height":"50%","left":"","top":"50%"}]}'
      ),
      array(
        'icon' => 'four_boxes.png',
        'name' => 'Four Boxes',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"","height":"50%","left":"","top":"0px","childs":[{"width":"50%","height":"","left":"0px","top":""},{"width":"50%","height":"","left":"50%","top":""}]},{"width":"","height":"50%","left":"","top":"50%","childs":[{"width":"50%","height":"","left":"0px","top":""},{"width":"50%","height":"","left":"50%","top":""}]}]}'
      ),
      array(
        'icon' => 'advanced.png',
        'name' => 'Advanced grid',
        'json' => '{"width":"100%","height":"100%","left":"","top":"","childs":[{"width":"50%","height":"","left":"0px","top":"","childs":[{"width":"","height":"75.72%","left":"","top":"0px"},{"width":"","height":"24.28%","left":"","top":"75.72%"}]},{"width":"50%","height":"","left":"50%","top":"","childs":[{"width":"","height":"34.48%","left":"","top":"0px"},{"width":"","height":"65.52%","left":"","top":"34.48%","childs":[{"width":"","height":"50%","left":"","top":"0px"},{"width":"","height":"50%","left":"","top":"50%"}]}]}]}'
      )
    );
  }


  /**
   * build_html builds html that will be rendered in our custom metabox
   */
  private function build_html(){
    $options = $this->get_templates_list();
    echo '<div class="cme-grid__wrp-box">';
    echo '<h3>' . _('Select one of the predefined grids or build your own!') . '</h3>';
    echo '<ul data-default-grids class="cme-grid__templates-list">';
    foreach ($options as $key => $option) {
      echo '<li class="cme-grid__templates-item">';
        echo '<input data-button-cme-grid id="cme-grid-' . $key . '" type="radio" name="cme-grid-option" value=' . $option['json'] . '>';
        echo '<label class="cme-grid__label" for="cme-grid-' . $key . '"> <img class="cme-grid__label-img" src="' . $this->icon_path . $option['icon'] . '" alt="' . _($option['name']) . '" />' . _($option['name']) . '</label>';
      echo '</li>';
    }
    echo '</ul>';
    echo '</div>';
  }
};
