(function($) {

  var AdminBox = function() {

    /**
     * SELECTORS description General Selectors
     *
     * @type {Object}
     */
    this.SELECTORS = {
      media: {
        addImage: '[data-add-image]',
        removeImage: '[data-rm-image]',
        gridParentBox: '.cme-grid__box',
        removeBox: '[data-rm-box]',
        template: '#template',
        img: '.img'
      },
      separators: {
        horizontal: {
          selector: '[data-sep-h]',
          axis: 'x',
        },
        vertical: {
          selector: '[data-sep-v]',
          axis: 'y',
        }
      },
      draggers: {
        horizontal: {
          selector: '.horizontal-drag',
          axis: 'y'
        },
        vertical: {
          selector: '.vertical-drag',
          axis: 'x'
        }
      },
      gridBuilder: {
        container: '[data-default-grids]',
        button: '[data-button-cme-grid]',
      },
      zoomer: {
        in: '[data-zoom-in]',
        out: '[data-zoom-out]'
      },
      clipboard: {
        container: '[data-copy-clipboard]',
        alertBox: '[data-alert-box]',
        success: 'Shortcode Copied to clipboard!',
        fail: 'Please copy this and insert into a post content!'
      },
      cropper: {
        button: '[data-crop-option]'
      }
    };
  };


  /**
   * init constructor function
   */
  AdminBox.prototype.init = function() {
    this.wrapper = $('#wrap');
    this.container = $('#cme-grid-container');
    this.jsonInput = $('#jsonData');
    this.resizer = $('#resizer');
    this.templateContainer = $('#template');
    this.post_id = this.container.data('postid');
    this.file_frame = false;
    this.currentBox = false;

    this.bindEvents();
  };

  /**
   * bindEvents function which is responsible for event binding
   */
  AdminBox.prototype.bindEvents = function() {
    var self = this;

    $.each(this.SELECTORS.draggers, function(i, dragger) {
      $(dragger.selector).each(function(index, el) {
        self.dragBorders.call(self, el, dragger.axis, el.parentNode);
      });
    });


    $.each(this.SELECTORS.separators, function(i, separator) {
      self.container.on(
        'click',
        separator.selector,
        self.createBoxes.bind(self, separator.axis)
      );
    });


    $(this.SELECTORS.media.img).each(function(index, el) {
      self.dragImages.call(self, el);
    });


    this.wrapper.on(
      'mouseenter',
      this.SELECTORS.media.removeBox,
      function() {
        $(this).parent().parent().parent().addClass('alert');
      }
    ).on(
      'mouseleave mouseup',
      this.SELECTORS.media.removeBox,
      function() {
        $(this).parent().parent().parent().removeClass('alert');
      }
    ).on(
      'click',
      this.SELECTORS.media.removeBox,
      this.removeBox.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.media.addImage,
      this.createModal.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.media.removeImage,
      this.removeImage.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.gridBuilder.button,
      this.rebuildGrid.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.zoomer.in,
      this.zoom.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.zoomer.out,
      this.zoom.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.clipboard.container,
      this.selectText.bind(this)
    )
    .on(
      'click',
      this.SELECTORS.cropper.button,
      this.adjustLayout.bind(this)
    );
  };

  /**
   * selectText responsible for selecting text from clicked elemnt
   *
   * @param  {event} e click event object
   */
  AdminBox.prototype.selectText = function(e) {
    var range;
    if (document.selection) {
      range = document.body.createTextRange();
      range.moveToElementText(e.target);
      range.select();
    } else if (window.getSelection) {
      range = document.createRange();
      range.selectNode(e.target);
      window.getSelection().addRange(range);
    }

    if (document.queryCommandSupported('copy')) {
      this.copyToClipboard.call(this);
    }else {
      this.alert.call(this, this.SELECTORS.clipboard.fail);
    }
  };


  /**
   * copyToClipboard function responsible for copying to clipboard passed value
   */
  AdminBox.prototype.copyToClipboard = function() {
    document.execCommand('copy');
    this.alert.call(this, this.SELECTORS.clipboard.success);
  };


  /**
   * alert function to alert the user with a custom message
   *
   * @param  {String} message to be appended into the the alertbox
   */
  AdminBox.prototype.alert = function(message) {
    var alertBox = $(this.SELECTORS.clipboard.alertBox);
    alertBox.html(message).slideDown(300);

    setTimeout(function() {
      alertBox.slideUp(300);
    }, 2000);
  };


  /**
   * zoom is responsible for zooming in or out a certain object
   *
   * @param  {Event} e click event object
   */
  AdminBox.prototype.zoom = function(e) {
    var img = $(e.target).parent().parent().find(this.SELECTORS.media.img);
    var currentZoom = this.getCurrentZoom(img);
    var newSize;

    if (isNaN(currentZoom)) {
      currentZoom = 100;
    }

    if (e.target.hasAttribute('data-zoom-in')) {
      currentZoom += 10;
    }else {
      if (currentZoom > 100) {
        currentZoom -= 10;
      }
    }
    newSize = currentZoom + '% auto';
    img.css('background-size', newSize);

    this.saveData.call(this);
  };


  /**
   * getCurrentZoom getCurrent Object zoom
   *
   * @param  {DomElement} element element to get the zoom value
   * @return {Number} current Zoom Value
   */
  AdminBox.prototype.getCurrentZoom = function(element) {
    var zoom = parseInt(element.css('background-size'));
    return zoom;
  };


  /**
   * rebuildGrid function responsible for rebuilding the grid after clicking
   * a certain template
   *
   * @param  {Event} click event
   */
  AdminBox.prototype.rebuildGrid = function(event) {
    var value = event.target.value;
    var jsonObject = JSON.parse(value);
    this.jsonInput.val(value);

    var axis = this.getAxis(jsonObject.childs);

    this.createMainBox.call(this, jsonObject);
  };


  /**
   * createMainBox creates main container where all the boxes will be added
   *
   * @param  {Object} data parsed JSON with data
   */
  AdminBox.prototype.createMainBox = function(data) {
    var boxTemplate = this.templateContainer
        .children(this.SELECTORS.media.gridParentBox)
        .clone();

    if (parseInt(data.height) > 100) {
      this.container.css('padding-bottom', data.height);
      data.height = '100%';
    }else {
      this.container.css('padding-bottom', data.height);
    }

    boxTemplate.css({
      left: data.left,
      top: data.top,
      width: data.width,
      height: data.height
    });

    this.container.empty().append(boxTemplate);

    if (data.childs) {
      this.parseChilds.call(this, boxTemplate, data.childs);
    }
  };


  /**
   * parseChilds function responsible for building one box at a time with all
   * necessary elements
   *
   * @param  {DOMElement} parent box where built box will be added
   * @param  {Array} items childs which will be added into the container
   */
  AdminBox.prototype.parseChilds = function(parent, items) {
    var self = this;
    var axis = this.getAxis(items);
    var controls;


    parent.empty();

    if (axis == 'x') {
      controls = this.resizer.children('.horizontal-drag').clone();
      controls.css('top', items[1].top);
    }else {
      controls = this.resizer.children('.vertical-drag').clone();
      controls.css('left', items[1].left);
    }


    $.each(items, function(index, element) {
      var boxTemplate = self.templateContainer
          .children('.cme-grid__box').clone();

      boxTemplate.css({
        left: element.left,
        top: element.top,
        width: element.width,
        height: element.height
      });
      parent.append(boxTemplate);

      if (element.childs) {
        self.parseChilds.call(self, boxTemplate, element.childs);
      }
    });
    parent.append(controls);
    this.dragBorders.call(this, controls[0], axis, parent[0]);
  };


  /**
   * getAxis getter which determines on what Axis the element is set on
   *
   * @param  {Array} items boxes between wich the axis is determined
   * @return {String}  return the axis of the boxes
   */
  AdminBox.prototype.getAxis = function(items) {
    var axis = 'x';

    if (items[0].left !== '' && items[0].width !== '') {
      axis = 'y';
    }

    return axis;
  };


  /**
   * removeImage removes selected image
   *
   * @param  {Event} event click event
   */
  AdminBox.prototype.removeImage = function(event) {
    event.preventDefault();
    $(event.target)
      .parent().parent().find('.img').remove();
    this.saveData.call(this);
  };


  /**
   * removeBox removes selected box and it's sibling
   *
   * @param  {Event} event click event object
   */
  AdminBox.prototype.removeBox = function(event) {
    event.preventDefault();
    var self = $(event.target);
    var box1 = this.templateContainer
        .children(this.SELECTORS.media.gridParentBox)
        .children()
        .clone();

    var parentBox = self.parent().parent().parent();

    parentBox.empty().append(box1);
    this.saveData.call(this);
  };


  /**
   * createBoxes determine box parent
   *
   * @param  {String} axis Axis based on which the element will be created
   * @return {Bool} false prevents default value for click button.
   */
  AdminBox.prototype.createBoxes = function(axis) {
    var parentBox = $(event.target).parent();
    this.appendBoxes.call(this, parentBox, axis);
    return false;
  };


  /**
   * createModal bind modal events
   *
   * @param  {Event} e click event object
   */
  AdminBox.prototype.createModal = function(e) {
    e.preventDefault();

    this.currentBox = $(e.target).parent().parent();

    // File Frame exists;
    if (!this.file_frame) {
      this.file_frame = this.createWPModal();
      this.file_frame.on(
        'select',
        this.appendImage.bind(this)
      );
    }
    this.file_frame.open();
  };


  /**
   * createWPModal creates Wordpress native modal Object
   *
   * @return {Object} wordpress modal object
   */
  AdminBox.prototype.createWPModal = function() {
    var modal = wp.media.frames.file_frame = wp.media({
      title: 'Upload File',
      button: {
        text: 'Select Image'
      },
      library: {
        type: 'image'
      },
      multiple: false
    });
    return modal;
  };


  /**
   * appendImage creates and appends to the container a selected image
   */
  AdminBox.prototype.appendImage = function() {
    var attachment = this.file_frame.state().get('selection').first().toJSON();
    var src = attachment.sizes['large'].url;
    var img = $('<span/>', {
      'class': 'img',
      'data-img-id': attachment.id
    });

    img.css('background-image', 'url(' + src + ')');
    this.currentBox.append(img);

    this.dragImages.call(this, img[0]);
    this.saveData.call(this);
  };


  /**
   * saveData creates a json and saves it into a hidden input
   */
  AdminBox.prototype.saveData = function() {
    var height = $(this.SELECTORS.cropper.button + ':checked').val() ||
        parseInt(this.container[0].style.paddingBottom) || 100;

    var dataObject = {};

    /**
     * parseChild recursive function to pass through the childs and get
     * existing data
     *
     * @param  {DOMElement} parent [description]
     * @return {Object} Object containing all the data from current boxes
     */
    function parseChild(parent) {
      var _return = null;

      $(parent).each(function() {
        var el = $(this);

        var dataElement = {
          width: el[0].style.width,
          height: el[0].style.height,
          left: el[0].style.left,
          top: el[0].style.top,
          bgPosition: el.find('>span.img').css('background-position'),
          bgSize: el.find('>span.img').css('background-size'),
          imgID: el.find('>span.img').data('img-id'),
        };

        if (el.find('.cme-grid__box').length > 0) {
          dataElement.childs = [];

          el.find('>.cme-grid__box').each(function() {
            dataElement.childs.push(parseChild($(this)));
          });
        }

        _return = dataElement;
      });
      return _return;
    }
    dataObject = parseChild(this.container.children());
    dataObject.containerHeight = height;

    var stringJson = JSON.stringify(dataObject);

    this.jsonInput.val(stringJson);
  };


  /**
   * dragImages a small constructor for image dragging only
   */
  AdminBox.prototype.dragImages = function() {
    var draggie = new Draggabilly(arguments[0]);

    var newX = 0,
      newY = 0,
      diffX, diffY;

    var dragMove = function(el) {
      var newPoints = el.element.style.transform.split('translate3d(');
      newPoints = newPoints[1].split(')');
      newPoints = newPoints[0].split('px,');

      var prevX = el.element.style.left;
      var prevY = el.element.style.top;


      newX = parseInt(newPoints[0] * -1) + parseInt(prevX, 10);
      newY = parseInt(newPoints[1] * -1) + parseInt(prevY, 10);

      if (newX > 100) {
        newX = 100;
      }else if (newX < 0) {
        newX = 0;
      }
      if (newY > 100) {
        newY = 100;
      }else if (newY < 0) {
        newY = 0;
      }
      el.element.style.backgroundPosition = newX + '% ' + newY + '%';
    };

    draggie.on('dragMove', dragMove);
    draggie.on('dragEnd', this.saveData.bind(this));
  };


  /**
   * dragBorders small Constructor which is responsible for resizing boxes
   *
   * @param  {String} selector  Element to be Selected
   * @param  {String} ax        Which axis the border will be moved
   * @param  {DOMElement} parent  Containig parent
   */
  AdminBox.prototype.dragBorders = function(selector, ax, parent) {
    var elem = $(selector)[0];
    var draggie = new Draggabilly(elem, {
      containment: parent
    });

    // onDrag Handler
    function onDragMove(instance, event, pointer) {
      var box1 = $(instance.element).siblings()[0];
      var box2 = $(instance.element).siblings()[1];

      var parentW = $(instance.element.parentElement).width() - 4;
      var parentH = $(instance.element.parentElement).height() + 10;

      var percentWX = (instance.position.x * 100) / parentW;
      percentWX = percentWX.toFixed(3);
      var diffX = 100 - percentWX;

      percentWX += '%';
      diffX += '%';

      var percentWY = (instance.position.y * 100) / parentH;
      percentWY = percentWY.toFixed(3);
      var diffY = 100 - percentWY;

      percentWY += '%';
      diffY += '%';

      if (instance.position.y > 5) {
        $(box1).css({
          'height': percentWY
        }).attr('data-box1-height', percentWY);

        $(box2).css({
          'top': percentWY,
          'height': diffY
        }).attr({
          'data-box2-top': percentWY,
          'data-box2-height': diffY
        });
      }else {
        $(box1).css({
          'width': percentWX,
        }).attr('data-box1-width', percentWX);

        $(box2).css({
          'left': percentWX,
          'width': diffX
        }).attr({
          'data-box2-left': percentWX,
          'data-box2-width': diffX
        });
      }
    }

    draggie.on('dragMove', onDragMove);
    draggie.on('dragEnd', this.saveData.bind(this));
  };


  /**
   * appendBoxes is responsible for creating two equal boxes when spliting one
   * box into two
   *
   * @param  {DOMElement} parentBox  Containig parent
   * @param  {String} axis         x or y
   */
  AdminBox.prototype.appendBoxes = function(parentBox, axis) {
    var boxTemplate = this.templateContainer
        .children(this.SELECTORS.media.gridParentBox);

    var box1 = boxTemplate.clone();
    var box2 = boxTemplate.clone();
    var controls;

    if (axis == 'x') {
      controls = this.resizer.children('.horizontal-drag').clone();
    }else {
      controls = this.resizer.children('.vertical-drag').clone();
    }

    if (axis == 'x') {
      box1.css({
        height: '50%',
        top: 0
      });
      box2.css({
        height: '50%',
        top: '50%'
      });
    }else {
      box1.css({
        width: '50%',
        left: 0
      });
      box2.css({
        width: '50%',
        left: '50%'
      });
    }

    // Appending created boxes
    parentBox.empty().append(box1).append(box2).append(controls);

    // Initializing dragger
    this.dragBorders.call(this, controls[0], axis, parentBox[0]);

    this.saveData.call(this);
  };


  /**
   * adjustLayout description sets Layout height
   *
   * @param  {Event} event mouse click event
   */
  AdminBox.prototype.adjustLayout = function(event) {
    var ratio = event.target.value + '%';
    this.container.css('padding-bottom', ratio);
    this.saveData.call(this);
    this.repositionHandlers.call(this);
  };


  AdminBox.prototype.repositionHandlers = function() {
    var distance = null;
    var horizontalDraggers = this.wrapper.find(
        this.SELECTORS.draggers.horizontal.selector
      );

    horizontalDraggers.each(function(i, hDrag) {
      distance = $(hDrag).siblings().eq(0).height() + 10;
      $(hDrag).css('top', distance);
    });
  };


  $(document).ready(function() {
    var adminBox = new AdminBox();
    adminBox.init();
  });
}(jQuery));
