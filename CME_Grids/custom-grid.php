<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/*
  Plugin Name: Custom Grid
  Plugin URI: http://example.com
  Version: 0.5
  Author: Constantin Melniciuc
  Description: This Plugin is created to offer users to create custom responsive grids.
  License:     GPL2
  License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/



/**
* Grid Plugin
*/
class CME_Grid {
  private $version;
  private $plugin_slug;
  private $plugin_path;
  private $options;


  /**
   * @constructor
   */
  public function __construct() {
    $this->version = '0.5';
    $this->plugin_slug = 'cme_grids';
    $this->plugin_path = plugin_dir_url( __FILE__ );
    $this->options = (object) array(
      'metabox' => '',
      'lightbox' => '',
      'lazyload' => '',
    );

    $this->include_dependencies();
    $this->add_hooks();

    $this->init_metaboxes();
  }


  /**
   * init_metaboxes additional metabox construtor
   */
  private function init_metaboxes() {
    $this->options->metabox = new CME_MetaBoxes();
    $this->options->default_grids = new CME_Default_Grids();
    $this->options->default_crop_options = new CME_Default_Crop();
  }


  /**
   * Custom Hooks on Wordpress actions
   */
  private function add_hooks() {
    add_action( 'init', array( $this, 'register_post_type') );
    add_action( 'add_meta_boxes', array( $this, 'register_meta_box') );
    add_shortcode( 'cme_grid', array( $this, 'shortcode_builder') );
    add_action( 'save_post', array( $this, 'cme_save_post') );
    add_action( 'admin_menu', array( $this, 'add_menu_page') );
    add_filter( 'body_class', array( $this, 'filter_body_classes' ));
  }


  /**
   * Include all plugin dependencies
   */
  private function include_dependencies() {
    require_once 'options/metabox-options.php';
    require_once 'options/default-grids.php';
    require_once 'options/crop.php';

    // Admin
    add_action( 'admin_enqueue_scripts', array( $this, 'register_plugin_admin_styles' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'register_plugin_admin_scripts' ) );

    // Front Queuing
    add_action( 'wp_enqueue_scripts', array( $this, 'register_front_scripts' ));
    add_action( 'wp_enqueue_scripts', array( $this, 'register_front_styles' ));
  }


  /**
   * Admin Style enquing
   */
  public function register_plugin_admin_styles() {
    wp_register_style( 'cme_styles', $this->plugin_path . 'dist/admin/css/main.css', array(), $this->version, false);
    wp_enqueue_style('cme_styles');
  }


  /**
   * Admin Scripts Enquing
   */
  public function register_plugin_admin_scripts() {
    wp_enqueue_media();

    wp_register_script( 'cme_scripts', $this->plugin_path . 'dist/admin/js/scripts.js', array(), $this->version, false );

    wp_enqueue_script( 'cme_scripts' );
  }


  /**
   * Front Scripts
   */
  public function register_front_scripts() {
    wp_register_script( 'cme_front_scripts', $this->plugin_path . 'dist/public/js/scripts.js', array(), $this->version, true );

    wp_enqueue_script( 'cme_front_scripts' );
  }


  /**
   * Front Styles
   */
  public function register_front_styles() {
    wp_register_style( 'cme_css', $this->plugin_path . 'dist/public/css/main.css', array(), $this->version, $media = 'all' );
    wp_enqueue_style( 'cme_css' );
  }


  /**
   * Custom Post Type Registration
   */
  public function register_post_type() {
    $labels = array(
      'name'               => _x( 'Grids', '' ),
      'singular_name'      => _x( 'Grid', '' ),
      'menu_name'          => _x( 'Grids', '' ),
      'name_admin_bar'     => _x( 'Grid', '' ),
      'add_new'            => _x( 'Add New', '' ),
      'add_new_item'       => __( 'Add New Grid' ),
      'new_item'           => __( 'New Grid' ),
      'edit_item'          => __( 'Edit Grid' ),
      'view_item'          => __( 'View Grid' ),
      'all_items'          => __( 'All Grids' ),
      'search_items'       => __( 'Search Grids' ),
      'parent_item_colon'  => __( 'Parent Grids:' ),
      'not_found'          => __( 'No grids found.' ),
      'not_found_in_trash' => __( 'No grids found in Trash.' )
    );

    $args = array(
      'labels'             => $labels,
      'public'             => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'cme_grids' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title', 'author'),
    );

    register_post_type( 'cme_grids', $args );
  }


  /**
   * Register Custom Metabox for Custom Post Type
   */
  public function register_meta_box() {

    $screens = array( $this->plugin_slug );

    foreach ( $screens as $screen ) {

      add_meta_box(
        'cmegrid_sectionid',
        __( 'Grid Editor', '' ),
        array( $this, 'custom_meta_box'),
        $screen
      );
    };
  }


  /**
   * Call to public shortcode
   * @param  array $atts shortcode options
   * @return html grid
   */
  public function shortcode_builder($atts) {
    ob_start();
    $this->build_front_grid($atts);
    $grid = ob_get_clean();
    return $grid;
  }


  /**
   * Builds custom grid based on the attributes provided
   * @param  array $options options for the
   */
  public function build_front_grid($options) {
    $grid = get_post_meta( $options['post_id'], 'cme_grid_data', TRUE);
    $grid = json_decode($grid);
    if (!isset($grid)) {
      return false;
    }
    $this->get_options($options['post_id']);

    $light_box = $this->options->lightbox;
    $lazy_load = $this->options->lazyload;

    echo "<div class='cme-grid__wrap' style='padding-bottom: " . $grid->containerHeight . "%'>";
    echo "<div class='cme-grid__box' style='width: ".$grid->width."; height: ".$grid->height."; left: ".$grid->left."; top: ".$grid->top.";'>";

    if (is_array($grid->childs)) {
      $sub_boxes = $this->recurse_boxes($grid->childs, $light_box, $lazy_load);
    }else {
      $img_src = wp_get_attachment_image_src( $grid->imgID, 'full');
      $ratio = ($img_src[2]/$img_src[1])*100;
      echo "<span class='cme-grid__img' style='background-image: url(\"".$img_src[0]."\"); background-position: ".$grid->bgPosition."; background-size: " . $grid->bgSize . "' data-ratio=\"" . $ratio . "\"></span>";
    }
    echo "</div></div>";
  }


  /**
   * get_options Getter for grid options
   * @param Int $post_id Post id of the post we're working with
   */
  private function get_options($post_id) {
    $this->options->lightbox = get_post_meta( $post_id, $this->options->metabox->meta_keys->lightbox, true );
    $this->options->lazyload = get_post_meta( $post_id, $this->options->metabox->meta_keys->lazyload, true );
  }


  /**
   * Single Container builder
   * @param  Array $box contains  data regarding the child withing each box
   * @param  Boolean $light_box   wether include lightbox option or not
   * @param  Boolean $lazy_load   wether include lazy loading option or not
   */
  private function recurse_boxes($box, $light_box, $lazy_load) {
    foreach ($box as $b) {
      echo "<div class='cme-grid__box' style='width: ".$b->width."; height: ".$b->height."; left: ".$b->left."; top: ".$b->top.";'>";
      if (!empty($b->imgID)) {
        $img_src = wp_get_attachment_image_src( $b->imgID, 'full');

        if (!$img_src) {
          return false;
        }

        $ratio = ($img_src[2]/$img_src[1])*100;

        if ($lazy_load == 'enable') {
          echo "<span class='cme-grid__img cme-grid__img--not-loaded' data-cme-unveil data-src=\"".$img_src[0]."\" style=\" background-position: ".$b->bgPosition."; background-size: " . $b->bgSize . "\" data-ratio=\"" . $ratio . "\"></span>";
        }else {
          echo "<span class='cme-grid__img' style=\"background-image: url('".$img_src[0]."'); background-position: ".$b->bgPosition."; background-size: " . $b->bgSize . "\" data-ratio=\"" . $ratio . "\"></span>";
        }
        if ($light_box == 'enable') {
          $img_data = get_post( $b->imgID );
          echo
            '<div class="cme-grid__info-box">
              <div class="cme-grid__mid-container">
                <a class="cme-grid__anchor" data-cme-lightbox rel="gallery1" href="'.$img_src[0].'"  title="'.$img_data->post_title.'">'. _('View') .'</a>
              </div>
            </div>';
        }
      }

      if (!empty($b->childs)) {
        $this->recurse_boxes($b->childs, $light_box, $lazy_load);
      }
      echo "</div>";
    }
  }


  /**
   * Admin Metabox visual
   * @param  Object $post default WP_Post Object
   */
  public function custom_meta_box($post) {
    $existing_grid = get_post_meta( $post->ID, 'cme_grid_data', TRUE );
    $grid = json_decode( $existing_grid );

    ?>
    <div id="wrap" class="cme-grid">
      <?php $this->options->default_grids->show_templates(); ?>
      <?php $this->options->default_crop_options->show_crop_options(); ?>

      <div class="cme-grid__wrp-box">
        <h1 class="cme-grid__shortcode">
          <span class="cme-grid__caps">shortcode:</span>
          <span class="cme-grid__shortcode-value" data-copy-clipboard>
            <?php $shortcode = '[cme_grid post_id="'.$post->ID.'"]';
              echo $shortcode;
            ?>
            <span data-alert-box class="cme-grid__alert-box"></span>
          </span>
        </h1>
      </div>

      <div class="cme-grid__templates">
        <div id="resizer">
          <div class="cme-grid__vertical-drag vertical-drag"></div>
          <div class="cme-grid__horizontal-drag horizontal-drag"></div>
        </div>
        <div id="template">
          <div class="cme-grid__box">
            <div class="cme-grid__buttons_grouped">
              <button type="button" class="cme-grid__button cme-grid__add-image" data-add-image></button>
              <button type="button" class="cme-grid__button cme-grid__rm-image" data-rm-image></button>
              <button type="button" class="cme-grid__button cme-grid__zoom-in" data-zoom-in></button>
              <button type="button" class="cme-grid__button cme-grid__zoom-out" data-zoom-out></button>
              <button type="button" class="cme-grid__button cme-grid__remove-box" data-rm-box></button>
            </div>
            <div class="cme-grid__sep-v" data-sep-v></div>
            <div class="cme-grid__sep-h" data-sep-h></div>
          </div>
        </div>
      </div>

      <div class="cme-grid__wrp-box cme-grid__wrp-box--uniform">
        <div id="cme-grid-container" class="cme-grid__wrap" data-postid="<?php echo $post->ID ?>" style="padding-bottom: <?php echo $grid->containerHeight; ?>%">
          <?php if (empty($grid)): ?>
            <div class="cme-grid__box" style="width: 100%; height: 100%;">
              <div class="cme-grid__sep-v" data-sep-v></div>
              <div class="cme-grid__sep-h" data-sep-h></div>
            </div>
          <?php else: ?>
            <div class="cme-grid__box" style="width: <?php echo $grid->width ?>; height: <?php echo $grid->height ?>;">
              <?php if (is_array($grid->childs)): ?>
                <?php $this->recurse_admin_boxes($grid->childs); ?>
              <?php else: ?>
                <div class="cme-grid__buttons_grouped">
                  <button type="button" class="cme-grid__button cme-grid__add-image" data-add-image></button>
                  <button type="button" class="cme-grid__button cme-grid__rm-image" data-rm-image></button>
                  <button type="button" class="cme-grid__button cme-grid__zoom-in" data-zoom-in></button>
                  <button type="button" class="cme-grid__button cme-grid__zoom-out" data-zoom-out></button>
                </div>
                <div class="cme-grid__sep-v" data-sep-v></div>
                <div class="cme-grid__sep-h" data-sep-h></div>
              <?php endif ?>
            </div>
          <?php endif; ?>
        </div>
        <input id="jsonData" type="hidden" name="cme_grid_data" value='<?php echo $existing_grid ?>'/>
      </div>
    </div>
    <?php
  }


  /**
   * Single Admin Container builder
   * @param  Array $box contains inforamtion about child boxes
   */
  private function recurse_admin_boxes($box) {
    $item_nr = 0;
    foreach ($box as $b): ?>

      <?php if ($item_nr == 1): ?>
        <?php if (empty($b->left)): ?>
          <div class="cme-grid__horizontal-drag horizontal-drag" style="top: <?php echo $b->top; ?>"></div>
        <?php else: ?>
          <div class="cme-grid__vertical-drag vertical-drag" style="left: <?php echo $b->left; ?>"></div>
        <?php endif ?>
      <?php endif; $item_nr++; ?>



      <div class="cme-grid__box" style="width: <?php echo $b->width ?>; height: <?php echo $b->height ?>; left: <?php echo $b->left ?>; top: <?php echo $b->top; ?>">
        <?php if (isset($b->childs) && is_array($b->childs)): ?>
          <?php $this->recurse_admin_boxes($b->childs); ?>
        <?php else: ?>
          <div class="cme-grid__buttons_grouped">
            <button type="button" class="cme-grid__button cme-grid__add-image" data-add-image></button>
            <button type="button" class="cme-grid__button cme-grid__rm-image" data-rm-image></button>
            <button type="button" class="cme-grid__button cme-grid__zoom-in" data-zoom-in></button>
            <button type="button" class="cme-grid__button cme-grid__zoom-out" data-zoom-out></button>
            <button type="button" class="cme-grid__button cme-grid__remove-box" data-rm-box></button>
          </div>
          <div class="cme-grid__sep-v" data-sep-v></div>
          <div class="cme-grid__sep-h" data-sep-h></div>
          <?php if (!empty($b->imgID)): ?>
            <?php $img = wp_get_attachment_image_src( $b->imgID, 'large' ); ?>
            <span class='img' data-img-id="<?php echo $b->imgID ?>" style="background-image: url('<?php echo $img[0] ?>'); background-position: <?php echo $b->bgPosition ?>; background-size: <?php echo $b->bgSize; ?>;">
          <?php endif ?>
        <?php endif ?>
      </div>
    <?php endforeach;
  }


  /**
   * Custom Save Post Hook
   * @param  Integer $post_id     PostId
   */
  public function cme_save_post( $post_id ) {
    $post = get_post( $post_id );

    // unhook this function so it doesn't loop infinitely
    remove_action( 'save_post', array( $this, 'cme_save_post') );

    if ($post->post_type != $this->plugin_slug) {
      return;
    }

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
      return;

    if ( !current_user_can( 'edit_posts' ) )
      return;

    $this->save_meta($post);
    // re-hook this function
    add_action( 'save_post', array( $this, 'cme_save_post') );
  }


  /**
   * Save additional meta data to the post object
   * @param  Object  $post    Default Wordpress Object
   */
  public function save_meta($post) {
    if (isset($_POST['cme_grid_data'])) {
      $jsonData = stripcslashes($_POST['cme_grid_data']);
      $prev_grid = get_post_meta( $post->ID, 'cme_grid_data', true );

      if (!empty($prev_grid)) {
        update_post_meta( $post->ID, 'cme_grid_data', $jsonData );
      }else {
        $added = add_post_meta( $post->ID, 'cme_grid_data', $jsonData, true );

        // In case the meta data was added as an empty string it evaluates to false,
        // it's trying to add post meta with a brand new key,
        // which is not allowed because of the last param
        // so we first remove all the meta_data with the same meta_key and add anew
        if (!$added) {
          $is_deleted = delete_post_meta( $post->ID, 'cme_grid_data');
          if ($is_deleted) {
            $re_added = add_post_meta( $post->ID, 'cme_grid_data', $jsonData, true );
          }
        }
      }

      $postarr = array(
        'ID' => $post->ID,
        'post_content' => '[cme_grid post_id="'.$post->ID.'"]'
      );
      wp_update_post( $postarr );
    }
  }


  /**
   * Create Custom Plugin Options
   */
  public function grid_setting() {
    //register our settings
    register_setting( 'cme-custom-grid-settings-group', 'cme_grids_disable_front_styles' );
    register_setting( 'cme-custom-grid-settings-group', 'cme_grids_drop_one_col_mobile' );
  }


  /**
   * Add Custom Admin Page
   */
  public function add_menu_page() {

    //create new top-level menu
    add_menu_page('Custom Grid Settings Page', 'Custom Grid Settings', 'administrator', __FILE__, array( $this, 'grid_options'), $this->plugin_path . 'dist/admin/img/cme_admin_logo.png' );

    //call register settings function
    add_action( 'admin_init', array( $this, 'grid_setting' ) );
  }


  /**
   * Create View for options page
   */
  public function grid_options() {
    ?>
    <div class="wrap">
      <h2>Custom Grid Options</h2>

      <form method="post" action="options.php">
        <?php settings_fields( 'cme-custom-grid-settings-group' ); ?>
        <?php do_settings_sections( 'cme-custom-grid-settings-group' );
          $front_styles = get_option( 'cme_grids_disable_front_styles' );
          $mob_col = get_option( 'cme_grids_drop_one_col_mobile' );
        ?>
        <table class="form-table">
          <tr valign="top">
            <th scope="row">Disable Front Styles</th>
            <td>
              <p>
                <input type="checkbox" name="cme_grids_disable_front_styles" value="1" <?php checked( 1 == $front_styles ); ?> />
                This option is intended for <strong>professionals</strong>. This option will <strong>remove</strong> all styling from the Open Lightbox button. Which will allow developers to customize the look and feel of the button.
              </p>
            </td>
          </tr>

          <tr valign="top">
            <th scope="row">Mobile 1 Column layout</th>
            <td>
              <p>
                <input type="checkbox" name="cme_grids_drop_one_col_mobile" value="1" <?php checked( 1 == $mob_col ); ?> />
                This option will transform your grid into 1 single column of images on mobile devices. The purpose of it, is to make the grids as accessible to all type of users as possible. As you can depending on the styling of your site, the mobile layout might be very narrow, and the grids inside will get extremely small and not user friendly. To improve that it is highly recommended to use this option. To create a way more user friendly look.
              </p>
            </td>
          </tr>
        </table>
        <?php submit_button(); ?>

      </form>
    </div>
    <?php
  }


  /**
   * [filter_body_classes description]
   * @param  array $classes default body classes for some page
   * @return array          additional classes which have to be added
   */
  public function filter_body_classes( $classes ) {
    $front_styles = get_option( 'cme_grids_disable_front_styles' );
    $mob_col = get_option( 'cme_grids_drop_one_col_mobile' );

    if ($front_styles) {
      array_push($classes, 'cme-grids-disabled');
    }

    if ($mob_col) {
      array_push($classes, 'cme-grids-one-col');
    }

    return $classes;
  }
}

// Initialization;
$cme_grid = new CME_Grid();
