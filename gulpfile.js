'use strict';

var PATHS = {
  src: {
    admin: {
      scss: './src/admin/scss/main.scss',
      scssWatch: './src/admin/scss/**/*.scss',
      js: './src/admin/js/**/*.js'
    },
    public: {
      scss: './src/public/scss/main.scss',
      scssWatch: './src/public/scss/**/*.scss',
      js: './src/public/js/**/*.js'
    }
  },

  out: {
    admin: {
      css: './dist/admin/css/',
      js: './dist/admin/js/'
    },
    public: {
      css: './dist/public/css/',
      js: './dist/public/js/'
    }
  }
};

var gulp = require('gulp'),
  notify = require('gulp-notify'),
  sass = require('gulp-sass'),
  plumber = require('gulp-plumber'),
  autoprefixer = require('gulp-autoprefixer'),
  rename = require('gulp-rename'),
  concat = require('gulp-concat'),
  livereload = require('gulp-livereload'),
  minifyCSS = require('gulp-minify-css'),
  minifyJS = require('gulp-js-minify');


var plumberError = function (err) {
  console.log(err);
  notify({message: err})
};

gulp.task('sass-admin', function(){
  return gulp.src(PATHS.src.admin.scss)
    .pipe(plumber(function (error) {
      plumberError(error);
      this.emit('end');
    }))
    .pipe(sass())
      .pipe(autoprefixer(
        'last 2 version',
        '> 1%',
        'ie 8',
        'ie 9',
        'ios 6',
        'android 4'
      ))
    .pipe(minifyCSS())
    .pipe(gulp.dest(PATHS.out.admin.css))
    .pipe(livereload())
    .pipe(notify({message: 'Admin SCSS!'}));
});

gulp.task('sass-public', function(){
  return gulp.src(PATHS.src.public.scss)
    .pipe(plumber(function (error) {
      plumberError(error);
      this.emit('end');
    }))
    .pipe(sass())
      .pipe(autoprefixer(
        'last 2 version',
        '> 1%',
        'ie 8',
        'ie 9',
        'ios 6',
        'android 4'
      ))
    .pipe(minifyCSS())
    .pipe(gulp.dest(PATHS.out.public.css))
    .pipe(livereload())
    .pipe(notify({message: 'Public SCSS!'}));
});

gulp.task('js-admin', function () {
  return gulp.src(PATHS.src.admin.js)
    .pipe(concat('scripts.js'))
    .pipe(minifyJS())
    .pipe(gulp.dest(PATHS.out.admin.js))
    .pipe(notify({message: 'Admin JS!'}));
});

gulp.task('js-public', function () {
  return gulp.src(PATHS.src.public.js)
    .pipe(concat('scripts.js'))
    // .pipe(minifyJS())
    .pipe(gulp.dest(PATHS.out.public.js))
    .pipe(notify({message: 'Public JS!'}));
});


// Default Task
gulp.task('default', ['sass-public', 'sass-admin', 'js-admin', 'js-public'], function () {
  livereload.listen();

  gulp.watch(PATHS.src.admin.scssWatch, ['sass-admin']);
  gulp.watch(PATHS.src.admin.js, ['js-admin'] );

  gulp.watch(PATHS.src.public.scssWatch, ['sass-public'] );
  gulp.watch(PATHS.src.public.js, ['js-public']);
});